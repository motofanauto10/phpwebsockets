<?php

namespace PhpWebSocket;

set_time_limit(0);

abstract class PhpWebSocket {
	
	//constansts
	const WS_MAX_CLIENTS = 200;
	const WS_MAX_CLIENTS_PER_IP = 100;
	
	//Server parameters
	protected $_sock;
	protected $_sock_clients = array();
	protected $_clients_data = array();
	protected $_clients_count = 0;
	protected $_clients_ip_count = array();
	protected $_stop = false;


	public function __construct($address, $port) {
		$this->createSocket();
		$this->socketBind($address, $port);
		$this->socketListen();
		echo "Server start listening\n";
		$this->doLoop();
	}
	
	protected function createSocket(){
		$this->_sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		
		if($this->_sock === false){
			throw new Exception("socket_create() error reason: " . socket_strerror(socket_last_error()));
		}
	}
	
	protected function socketBind($address, $port){
		if(socket_bind($this->_sock, $address, $port) === false){
			$er = socket_last_error($this->_sock);
			socket_close($this->_sock);
			throw new Exception("socket_bind() error reason: " . socket_strerror($er));
		}	
	}
	
	protected function socketListen(){
		if (socket_listen($this->_sock, 10) === false){
			$er = socket_last_error($this->_sock);
			socket_close($this->_sock);
			throw new Exception("socket_listen() error reason: " . socket_strerror($er));
		}	
	}
	
	protected function doLoop(){
		
		while(!$this->_stop){
			
			//Retrieve client messages
			$this->retrieveClientSocketsChanges();
			
			// function launched each iteration
			$this->callback();
		}
		$this->terminateServer();
	}
	
	protected function retrieveClientSocketsChanges(){
		
		$write = array();
		$except = array();
		$changed_clients = $this->_sock_clients;
		$changed_clients['socket_server'] = $this->_sock;
		$result = socket_select($changed_clients, $write, $except, 1);
		
		if ($result === false) {
			$this->closeAllClients();
			socket_close($this->_sock);
			throw new Exception("socket_select() error reason: " . socket_strerror(socket_last_error()));
		}
		
		if(isset($changed_clients['socket_server'])){
			$new_client = $this->checkNewClient();
			$this->addSocketClient($new_client);	
			unset($changed_clients['socket_server']);
		}
		
		if ($result > 0) {
			foreach ($changed_clients as $client_id => $socket) {
				
				
				// client socket changed
				$buffer = '';
				$bytes = @socket_recv($socket, $buffer, 4096, 0);

				if ($bytes === false || $bytes === 0) {
					// error on recv, remove client socket (will check to send close frame)
					$this->unsetClientDataAndSocket($client_id);
				}
				else {
	
					$this->processData($buffer, $client_id);
				}
			}
		}
	}

	protected function unsetClientDataAndSocket($client_id){
		
		$ip = $this->_clients_data[$client_id]['ip'];
		socket_close($this->_sock_clients[$client_id]);
		unset($this->_sock_clients[$client_id]);
		unset($this->_clients_data[$client_id]);
		$this->_clients_count--;
		$this->_clients_ip_count[$ip]--;
	}
	
	protected function checkNewClient(){
		
		$client_sock = socket_accept($this->_sock);
		if($client_sock === false){
			$er = socket_last_error($this->_sock);
			$this->closeAllClients();
			socket_close($this->_sock);
			throw new Exception("socket_accept() error reason: " . socket_strerror($er));
		}
		
		return $client_sock;
	}

	protected function addSocketClient($sock){
		
		// fetch client IP as integer. &$client_ip data by reference
		$result = socket_getpeername($sock, $client_ip);
		$success = true;
		if($result === false){
			$success = false;
		}
		$client_ip = sprintf('%u', ip2long($client_ip));

		if ($this->_clients_count == self::WS_MAX_CLIENTS){
			$success = false;
		}
		if((isset($this->_clients_ip_count[$client_ip]) && $this->_clients_ip_count[$client_ip] == self::WS_MAX_CLIENTS_PER_IP)) {
			$success = false;
		}
		if($success){
			
			$unique_id = md5(uniqid(rand(1000,9999), true));
			$this->_sock_clients[$unique_id] = $sock;
			$this->_clients_data[$unique_id] = array(
				'socket'		=> $sock,
				'last_change'	=> time(),
				'ip'			=> $client_ip);
			$this->_clients_count++;
			$this->_clients_ip_count[$client_ip] = !isset($this->_clients_ip_count[$client_ip])? 1 : ($this->_clients_ip_count[$client_ip] + 1);
			echo "New client $unique_id ip: $client_ip\n";
		}else{
			socket_close($sock);
			return false;
		}
	}

	protected function sendClientMessage($client_id, $msg){
		
		if(isset($this->_clients_data[$client_id]['handshake_accepted'])){
			$msg = $this->hybi10Encode($msg);
			var_dump($msg);
		}
		if($socket = $this->_sock_clients[$client_id]){
			$result = socket_write($socket, $msg);
			if($result === false){
				$this->unsetClientDataAndSocket($client_id);
			}
		}
	}


	protected function terminateServer(){
		$this->closeAllClients();
		socket_close($this->_sock);
		echo "Server terminated\n";
	}
	
	protected function closeAllClients(){
		
		foreach ($this->_sock_clients as $client_id => $socket) {
			$this->sendClientMessage($client_id, 'server terminated');
			$this->unsetClientDataAndSocket($client_id);
		}
	}
	
	public function broadcastMsg($msg, $excep_client_id = false){
		
		foreach ($this->_sock_clients as $client_id => $socket) {
			if($client_id == $excep_client_id){
				continue;
			}
			$this->sendClientMessage($client_id, $msg);
		}
	}
	
	protected function sendWebsocketHandShake($data, $client_id){
		//debug
		// Data example:
		//	Sec-WebSocket-Key: 0mHaiztPLfPyx59eJPnzOw==
		//
		preg_match('/Sec\-WebSocket\-Key:\s*([^\n]*)/', $data, $matches);

		$key = (isset($matches[1])) ? trim($matches[1]) : null;
		
		if($key){
			
			// work out hash to use in Sec-WebSocket-Accept reply header
			$hash = base64_encode(sha1($key.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11', true));
			// build headers
			$headers = array(
				'HTTP/1.1 101 Switching Protocols',
				'Upgrade: websocket',
				'Connection: Upgrade',
				'Sec-WebSocket-Accept: '.$hash
			);
			$headers = implode("\r\n", $headers)."\r\n\r\n";

			// send headers back to client
			$socket = $this->_clients_data[$client_id]['socket'];

			$left = strlen($headers);
			do {
				$sent = @socket_send($socket, $headers, $left, 0);
				if ($sent === false) {
					$this->unsetClientDataAndSocket($client_id);
				}

				$left -= $sent;
				if ($sent > 0) $headers = substr($headers, $sent);
			}
			while ($left > 0);
			
			//Enable handshaked socket
			$this->_clients_data[$client_id]['handshake_accepted'] = true;
			
			return true;
		}
		return false;
		
	}
	
	protected function processData($data, $sender_id){

		if(isset($this->_clients_data[$sender_id]['handshake_accepted'])){
			return $this->processDataHandshaked($data, $sender_id);
		}
		
		$data = trim($data);
		
		switch ($data) {
			case 'shutdown':
				$this->_stop = true;
				break;
			case 'quit':
				$this->sendClientMessage($sender_id, "exit\n");
				$this->unsetClientDataAndSocket($sender_id);
				echo "$sender_id exit!\n";
				break;
			case (preg_match('/broadcast/', $data)?true:false):
				$this->broadcastMsg($data, $sender_id);
				break;
			case (preg_match('/Sec\-WebSocket\-Key:/', $data)?true:false):
				$this->sendWebsocketHandShake($data, $sender_id);
				break;
			default:
				echo "$sender_id says: $data\n";
				break;
		}
	}

	protected function processDataHandshaked($data, $sender_id){
		
		$decoded_data = $this->hybi10Decode($data);
		
		if(!isset($decoded_data['type'])) return false;
		
			switch($decoded_data['type']) {
				case 'error':
					return false;
					break;

				case 'ping':
					echo "Ping received.\n";
					break;

				case 'pong':
					echo "pong received.\n";
					break;

				case 'text':
					//TODO: process data sended by client in Web Navigator
					var_dump($decoded_data);
					
					break;
		}

		return true;
	}
	
	protected function hybi10Decode($data) {
		
		$payloadLength = '';
		$mask = '';
		$unmaskedPayload = '';
		$decodedData = array();

		// estimate frame type:
		$firstByteBinary = sprintf('%08b', ord($data[0]));
		$secondByteBinary = sprintf('%08b', ord($data[1]));
		$opcode = bindec(substr($firstByteBinary, 4, 4));
		$isMasked = ($secondByteBinary[0] == '1') ? true : false;
		$payloadLength = ($isMasked === true) ? ord($data[1]) & 127 : ord($data[1]);

		// close connection if unmasked frame is received:
		if ($isMasked === false) {
			$this->close(1002);
		}

		switch ($opcode) {
		// text frame:
			case 1:
				$decodedData['type'] = 'text';

				if ($payloadLength === 126) {
					$mask = substr($data, 4, 4);
					$payloadOffset = 8;
					$dataLength = bindec(sprintf('%08b', ord($data[2])) . sprintf('%08b', ord($data[3]))) + $payloadOffset;
				} elseif ($payloadLength === 127) {
					$mask = substr($data, 10, 4);
					$payloadOffset = 14;
					$tmp = '';
					for ($i = 0; $i < 8; $i++) {
						$tmp .= sprintf('%08b', ord($data[$i + 2]));
					}
					$dataLength = bindec($tmp) + $payloadOffset;
					unset($tmp);
				} else {
					$mask = substr($data, 2, 4);
					$payloadOffset = 6;
					$dataLength = $payloadLength + $payloadOffset;
				}

				$dataLength = strlen($data);
				for ($i = $payloadOffset; $i < $dataLength; $i++) {
					$j = $i - $payloadOffset;
					$unmaskedPayload .= $data[$i] ^ $mask[$j % 4];
				}

				$decodedData['payload'] = $unmaskedPayload;
				break;

			// connection close frame:
			case 8:
				$decodedData['type'] = 'close';
				break;

			// ping frame:
			case 9:
				$decodedData['type'] = 'ping';
				break;

			// pong frame:
			case 10:
				$decodedData['type'] = 'pong';
				break;

			default:
			// @TODO: Close connection on unknown opcode.
				break;
		}

		return $decodedData;
	}


	protected function hybi10Encode($payload, $type = 'text', $masked = true) {
		
		$frameHead = array();
		$frame = '';
		$payloadLength = strlen($payload);

		switch ($type) {
			case 'ping':
				echo "Sending ping.\n";
				return chr(137) . chr(4) . 'ping';
				break;

			case 'pong':
				echo "Sending pong.\n";
				break;

			case 'text':
				$mask = array();
				$payloadLength = strlen($payload);

				// generate a random mask:
				for ($i = 0; $i < 4; $i++) {
					$mask[$i] = chr(rand(0, 255));
				}

				// first bit indicates FIN, Text-Frame:
				$frameHead[0] = 0x81;

				// set payload length (using 1, 3 or 9 bytes)
				if ($payloadLength > 65535) {
					$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
					$frameHead[1] = ($masked === true) ? 255 : 127;
					for ($i = 0; $i < 8; $i++) {
						$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
					}
					// most significant bit MUST be 0 (return false if to much data)
					if ($frameHead[2] > 127) {
						return false;
					}
				} elseif ($payloadLength > 125) {

					$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
					$frameHead[1] = ($masked === true) ? 254 : 126;
					$frameHead[2] = bindec($payloadLengthBin[0]);
					$frameHead[3] = bindec($payloadLengthBin[1]);
				} else {
					$frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
				}

				// convert frame-head to string:
				foreach (array_keys($frameHead) as $i) {
					$frameHead[$i] = chr($frameHead[$i]);
				}
				if ($masked === true) {
				// generate a random mask:
					$mask = array();
					for ($i = 0; $i < 4; $i++) {
						$mask[$i] = chr(rand(0, 255));
					}

					$frameHead = array_merge($frameHead, $mask);
				}
				$frame = implode('', $frameHead);

				// mask payload data and append to frame:
				$framePayload = array();
				for ($i = 0; $i < $payloadLength; $i++) {
					$frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
				}
				break;

			case 'close':
				break;
		}


		return $frame;
	}
	

	abstract function callback();
}
