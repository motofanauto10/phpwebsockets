PhpWebSocket, a simple socket class
===================================

How to use it:

server.php
----------

.. code-block:: php

	<?php

	require_once  '../../vendors/phpwebsocket/src/PhpWebSocket.php';

	class PhpWebSocketTest extends PHPWebSocket{
		public function callback() {

		}
	}

	new PhpWebSocketTest('127.0.0.1', 12345);


Connect with Telnet
-------------------

::

	telnet 127.0.0.1 12345

To shutdown the server

::

	shutdown

To kill the client

::

	quit

To broadcast message between clients

::

	broadcast hello clients!

